import Component from '@ember/component';
import { task } from 'ember-concurrency';
import { observer } from '@ember/object';
import moment from 'moment';
import { A } from '@ember/array';
import { storageFor } from 'ember-local-storage';
import { getOwner } from '@ember/application';
import { runTask, pollTask, cancelPoll, runDisposables } from 'ember-lifeline';
import { inject as service } from '@ember/service';
import { copy } from '@ember/object/internals';

export default Component.extend({
  init() {
    this._super(...arguments);
    this._pollToken = pollTask(this, 'keyUpWatcher');
    if (this.get("filterGrouping").length > 0) {
      this.setFilterGrouping();
    }
  },

  targetFilterValue: null,
  lastKeyUpTime: 0,
  lastKeyUpValue: null,
  keyUpDelay: 1250, // delay in ms
  keyUpInterval: 250, // interval for keyUpWatcher
  characterLimit: 3,
  enterKeyValue: 13,
  filterIconPosition: 'left',
  columnSize: 2,
  canSetObjects: true,
  currentSearch: null,
  user: null,
  group: null,
  savedSearches: null,
  savedSearchEnabled: true,
  savedSearchUserKey: 'eaUser',
  savedSearchGroupKey: 'trainingLocation',
  savedSearchToDeleteId: null,
  isClearing: false,
  isLoading: false,
  associateArrays: null,
  filterGrouping: [],
  displayUnfilteredAssociateArrays: true,

  appearance: storageFor('multisearch'),
  store: service('store'),

  loadSavedSearchesTask: task(function * () {
    let q = {};

    let userQueryKey = this.get('savedSearchUserKey') + 'Id';
    let groupQueryKey = this.get('savedSearchGroupKey') + 'Id';
    let route = getOwner(this).lookup('controller:application').currentPath;

    q[userQueryKey] = this.get('user.id');
    q['route'] = route;

    let savedSearches = yield this.get('store').query('saved-search', q);

    this.set('savedSearches', savedSearches)
  }).drop(),

  visibleFiltersObserver: observer('filters.@each.checked', function() {
    let currentPath = getOwner(this).lookup('controller:application').currentPath;
    currentPath = currentPath.replace(/\./g, '_');

    this.get('appearance').set(currentPath, this.get('filters').map(f => f.checked));
  }),

  keyUpWatcher(next) {
    let searchValue = this.get('targetFilterValue');
    let lastKeyUpTime = this.get('lastKeyUpTime');
    let lastKeyUpValue = this.get('lastKeyUpValue');
    let now = (new Date()).getTime();

    if ((!searchValue || searchValue && searchValue.length >= this.get('characterLimit')) &&
        (lastKeyUpTime && (now - lastKeyUpTime) > this.get('keyUpDelay')) && (lastKeyUpValue !=
        searchValue) && (lastKeyUpValue != this.get("enterKeyValue"))) {
      this.set('lastKeyUpTime', 0);
      this.applyFilters();
    }

    runTask(this, next, this.get('keyUpInterval'));
  },

  objectsObserver: observer('searchObjectsIn', 'searchObjectsIn.[]', 'searchObjectsIn.@each', function() {
    this.applyFilters();
  }),

  filtersObserver: observer('filters.@each.checkboxValue', 'filters.@each.selectValue', 'filters.@each.dateValue', function() {
    if (this.get('isClearing') || this.get('isLoading')) {
      return;
    }

    this.applyFilters();
  }),

  multiPropertyPartialStringMatchTask: task(function * (objects, filters) {
    let queryParamsToUpdate = {};

    if (this.get("associateArrays") !== null) {

      let shouldDisplayAssociateArrays = this.get("displayUnfilteredAssociateArrays");

      // By default of the associate arrays are not filtered they will display
      // but they can be turned off so this is checked to see if has been 
      if (!shouldDisplayAssociateArrays) {
        this.get("associateArrays").forEach(function(assocField) {

          // since we are not displaying unfilted associate arrays 
          // each filter for the assoicate array must be  checked to ensure that there is a 
          // a value for atleast one of the filters before the associate arrays are shown
          let filledOutAssociateFilters = filters.filter(function(associateFilter) {
            if (associateFilter.get("searchTarget") == assocField) {
              let associateFilterValue = associateFilter.getReadableValue();

              if (associateFilterValue !== null && associateFilterValue !== "") {
                return true;
              }
            }

            return false;
          }.bind(this));

          if (filledOutAssociateFilters.length > 0) {
            shouldDisplayAssociateArrays = true;
          }

        });
      }

      // restore all previously filtered associate arrays back to there original unfiltered state
      // or make them empty depending on shouldDisplayAssociateArrays value 
      objects.forEach(function(obj) {
        this.restoreAssociateArray(obj, shouldDisplayAssociateArrays);
      }.bind(this));

    }

    for (var i=0; i < filters.length; i++){
      var filter = filters.objectAt(i);

      if (filter.queryParam) {
        if (!this.get('setQueryParams')) {
          this.setQueryParam(filter.queryParam, filter.rawValue ? filter.getValue() : filter.getReadableValue());
        }

        queryParamsToUpdate[filter.queryParam] = filter.rawValue ? filter.getValue() : filter.getReadableValue();
      }

      if (filter.backEndFilter) {
        continue;
      }

      objects = yield objects.filter((obj) => {
        var v = obj.get(filter.get('searchTarget'));
        var searchValue = filter.getReadableValue();

        if (typeof searchValue === 'undefined' || searchValue === null ||
            (Array.isArray(searchValue) && searchValue.length === 0)) {

          return true;
        }

        if (typeof v === 'undefined' || v === null) {
          return false;
        }

        if (filter.type === 'text') {

          // Meaning we have a field that is an array of strings that we want to limit to only matches 
          if (filter.get("mutate")) {

            this.filterByTextMutate(obj, filter.get('searchTarget'), searchValue, filter.get("associateArrays"));
            // once the field is filtered if there are any actual matches 
            // we return true 
            if (obj.get(filter.get('searchTarget')).length > 0) {
              return true;
            }
            else {
              return false;
            }
          }
          else {
            return this.filterByText(v, searchValue);
          }

        }

        if (searchValue && filter.type === 'checkbox') {
          return this.filterByCheckbox(v);
        }

        if (filter.type === 'select') {
          return this.filterBySelect(v, searchValue);
        }

        if (Array.isArray(searchValue) && searchValue.length && filter.type === 'multiselect') {

          if(filter.get("isHasMany")){
            return this.filterByMultiselect(v, searchValue,filter.optionValuePath);
          }
          else{
            return this.filterByMultiselect(v, searchValue);
          }
        }


        if (filter.type === 'date') {
          let operation = filter.hasOwnProperty('operation') ? filter.operation : '=';

          // Meaning we have a field that is an array of dates that we want to limit to only matches 
          if (filter.get("mutate")) {

            // once the field is filtered if there are any actuald date matches 
            // we return true 
            this.filterByDateMutate(obj, filter.get('searchTarget'), searchValue, operation, filter.get("associateArrays"));
            if (obj.get(filter.get('searchTarget')).length > 0) {
              return true;
            }
            else {
              return false;
            }
          }

          if (filter.get("isHasMany")) {
            return this.filterByDate(v, searchValue, operation, filter.optionValuePath);
          }

          return this.filterByDate(v, searchValue, operation);
        }

        return true;
      });
    }

    if (this.get('setQueryParams') && Object.keys(queryParamsToUpdate).length > 0) {
      this.setQueryParams(queryParamsToUpdate);
    }

    if (objects) {
      this.set('searchObjectsOut', objects);

      if (this.get('canSetObjects')) {
        this.setFilteredObjects(objects);
        this.setItemCounts(objects.length, this.get('searchObjectsIn.length'));
      }
    }
  }).restartable(),


    /*
  * This is used to group filters together when they are displayed, so that filters that 
  * are in similar categories are displayed together 
  */
    setFilterGrouping() {
      let filterGroups = this.get("filterGrouping");

      filterGroups.forEach(function(filterGroup) {

        filterGroup.set("filters", []);
        // gets the array of searchTarget names for each group 
        filterGroup.get("grouping").forEach(function(filter) {
          // get all of the filters that have that searchTarget 
          let filters = this.get("filters").filterBy("groupTarget", filter);

          filters.forEach(function(filter) {
            // push that filter object to an array of filters objects for that group 
            // to be displayed later 
            filterGroup.get("filters").push(filter);
          }.bind(this));

        }.bind(this));
      }.bind(this));

    },

  /*
  * Used to restore array attributes in list objects that have been changed
  * when the mutate property for filtering is turned on. This function is rerun after 
  * any filters are changed. 
  */
  restoreAssociateArray(obj, shouldDisplayGroup) {

    let arrays = this.get("associateArrays");

    arrays.forEach(function(field) {

      if (typeof obj.get("unfiltered_" + field) == "undefined") {
        // store an unfiltered copy of array attribute to restore field to when a filter is changed 
        obj.set("unfiltered_" + field, JSON.parse(JSON.stringify(obj.get(field))));
      }

      // shouldDisplayGroup is passed to determine whether the associate arrays should be displayed 
      // this is useful for not displaying fields in list whose filters are not filled in 
      if (shouldDisplayGroup) {
        // set the original field back to its unfiltered self 
        obj.set(field, JSON.parse(JSON.stringify(obj.get("unfiltered_" + field))));
      }
      else {
        obj.set(field, []);
      }

    }.bind(this));
  },

  /*
  * Filters an object's array of text and mutates the list to show only matches 
  * based on the search target.
  */
  filterByTextMutate(obj, searchTarget, search, assocArrays) {
    let searchArray = obj.get(searchTarget);

    for (let i = 0; i < searchArray.length; i++) {
      let searchElement = searchArray[i];
      if (!((typeof searchElement == "string") && searchElement.toLowerCase().indexOf(search.toLowerCase()) !== -1)) {
        // since the element of the array does not match, it gets removed 
        searchArray.splice(i, 1);
        // remove all elements from associate arrays at the current index 
        this.mutateAssociatedArrays(assocArrays, obj, i);
        i--;
      }

    }
  },

  /*
  * Removed elements from arrays that are associaed with each other once 
  * one of the arrays are mutated.
  * assocArrays - list of arrays that are associated with the array that has been filtered 
  * obj - the object in the list that is currently being searched 
  * indexToRemove - the index that is used to remove the element from each of the arrays 
  */
  mutateAssociatedArrays(assocArrays, obj, indexToRemove) {
    assocArrays.forEach(function(assocField) {
      let assocArray = obj.get(assocField);
      assocArray.splice(indexToRemove, 1);
    }.bind(this));

  },


  filterByText(value, search) {
    return value.toLowerCase().indexOf(search.toLowerCase()) !== -1;
  },

  filterByCheckbox(value) {
    var isTruthy = false;

    if (typeof value === 'string' || Array.isArray(value)) {
      isTruthy = value.length > 0;
    } else if (typeof value === 'number') {
      isTruthy = value > 0;
    } else if (typeof value === 'boolean') {
      isTruthy = value === true;
    }

    return isTruthy;
  },

  filterBySelect(value, search) {
    return value === search;
  },

  filterByMultiselect(value, searches, searchField) {
     if(searchField){

        for(var i = 0; i < value.length; i++){
          var currVal = value.objectAt(i).get(searchField);

          if(searches.includes(currVal)){
            return true;
          }
        }

        return false;
     }

     else
       return searches.includes(value);
   },

    /*
      * Filters an object's array of dates and mutates the list to show only matches
      * based on the search criteria. 
      */

    filterByDateMutate(obj, searchField, search, operation, assocArrays) {
      let dates = obj.get(searchField);

      for (var i = 0; i < dates.length; i++) {
        var currDate = dates[i];

        let date = !isNaN(currDate) ? moment.unix(currDate) : moment(currDate);

        if (!this.compareDates(date, search, operation)) {
          // since the element of the array does not match, it gets removed 
          dates.splice(i, 1);
          // remove all elements from associate arrays at the current index 
          this.mutateAssociatedArrays(assocArrays, obj, i);
          i--;
        }
      }
    },

  filterByDate(value, search, operation, searchField) {
    if(searchField){
       for(var i = 0; i < value.length; i++){
         var currDate = value.objectAt(i).get(searchField);
         let date = !isNaN(currDate) ? moment.unix(currDate) : moment(currDate);

         if(this.compareDates(date,search,operation)){
           return true;
         }
       }
       return false;
    }

    let date = !isNaN(value) ? moment.unix(value) : moment(value);
    return this.compareDates(date,search,operation)

  },

  applyFilters: function() {
    var objects = [];

    // only for front-end filters, not necessary for back-end filters
    if (this.get('canSetObjects')) {
      objects = this.get('searchObjectsIn').filter(obj => obj.get('isDeleted') !== true);
    }

    this.get('multiPropertyPartialStringMatchTask').cancelAll();
    this.get('multiPropertyPartialStringMatchTask').perform(objects, this.get('filters'));
  },

  willDestroyElement() {
    this._super(...arguments);

    // finish polling for keyUp events
    cancelPoll(this, 'keyUpWatcher');

    // clear filters
    this.send('clear', false);

    // remove objects from the search arrays
    this.set('searchObjectsIn', []);
    this.set('searchObjectsOut', []);

    // update controller with empty array
    if (this.get('canSetObjects')) {
      this.setFilteredObjects([]);
    }
  },

  willDestroy() {
    this._super(...arguments);

    runDisposables(this);
  },

  didReceiveAttrs() {
    this._super(...arguments);

    let queryParams = this.get('queryParams');

    if (!Array.isArray(queryParams)) {
      return;
    }

    if (queryParams) {
      this.retrieveQueryParams();
    }
  },

  didInsertElement() {
    this._super(...arguments);

    let currentPath = getOwner(this).lookup('controller:application').currentPath;
    currentPath = currentPath.replace(/\./g, '_');

    this.get('loadSavedSearchesTask').perform();

    let storedFilterOptions = this.get('appearance.' + currentPath);

    if (storedFilterOptions) {
      storedFilterOptions.forEach((f, index) => {
        let filter = this.get('filters').objectAt(index);

        if (filter) {
          filter.set('checked', f);
        }
      });
    }

    this.applyFilters();
  },


  compareDates(date, search, operation){
    if (operation === '<') {
      return moment(search).isAfter(date);
    } else if (operation === '>') {
      return moment(search).isBefore(date);
    }
    return moment(search).isSame(date);
  },

  actions: {
    saveCurrentSearch: function() {
      if (!this.get('currentSearch.id')) {
        let route = getOwner(this).lookup('controller:application').currentPath;
        const filters = this.get('filters');

        let clonedFilters = filters.map(f => {
          return f.toObject();
        });

        console.log(clonedFilters);

        let currentSearch = this.get('store').createRecord('saved-search', {
          route: route,
          searchData: JSON.stringify(clonedFilters)
        });

        currentSearch.set(this.get('savedSearchUserKey'), this.get('user'));
        currentSearch.set(this.get('savedSearchGroupKey'), this.get('group'));

        this.set('currentSearch', currentSearch);
      }

      $('#saveSearchModal').modal('show');
    },
    saveCurrentSearchSuccess: function() {
      this.get('currentSearch').save().then(() => {
        this.set('currentSearch', null);
        this.get('savedSearches').update();
        this.toast.success('Successfully saved the saved search.');
      });

      $('#saveSearchModal').modal('hide');
    },
    saveCurrentSearchCancel: function() {
      $('#saveSearchModal').modal('hide');
    },
    loadSavedSearch: function(savedSearch) {
      this.set('isLoading', true);

      let savedFilters = JSON.parse(savedSearch.get('searchData'));

      let indicesList1 = savedFilters.mapBy('searchPlaceHolder');
      let indicesList2 = savedFilters.mapBy('alternateLabel');

      let filters = this.get('filters');

      filters = filters.map(f => {
        let displayName = f.get('displayName');
        let index = indicesList1.indexOf(displayName);

        if (index < 0) {
          index = indicesList2.indexOf(displayName);
        }

        if (index >= 0) {
          let valueKey = f.getValueKey();
          let savedFilter = savedFilters.objectAt(index);

          f.loadFilter(savedFilter);
          f.setValue(savedFilter[valueKey]);
        }

        return f;
      });

      this.set('filters', filters);
      this.set('currentSearch', null);
      this.set('isLoading', false);
      this.applyFilters();
    },
    manageSavedSearches: function() {
      $('#manageSavedSearchesModal').modal('show');
    },
    manageSavedSearchesClose: function() {
      $('#manageSavedSearchesModal').modal('hide');
    },
    deleteSavedSearch: function(id) {
      this.set('savedSearchToDeleteId', id);

      $('#manageSavedSearchesModal').modal('hide');

      $('#manageSavedSearchesModal').on('hidden.bs.modal', () => {
        $('#confirmDialog').modal('show');
        $('#manageSavedSearchesModal').off();
      });
    },
    confirmDeleteSavedSearch: function() {
      let id = this.get('savedSearchToDeleteId');

      if (id) {
        let savedSearch = this.get('store').peekRecord('saved-search', id);

        savedSearch.destroyRecord().then(() => {
          this.toast.success('Successfully deleted the saved search.');
          this.set('savedSearchToDeleteId', null);
        });
      }

      $('#confirmDialog').modal('hide');
    },
    renameSavedSearch: function(id) {
      let savedSearch = this.get('store').peekRecord('saved-search', id);

      if (savedSearch) {
        this.set('currentSearch', savedSearch);

        $('#manageSavedSearchesModal').modal('hide');

        $('#manageSavedSearchesModal').on('hidden.bs.modal', () => {
          $('#saveSearchModal').modal('show');
          $('#manageSavedSearchesModal').off();
        });
      }
    },
    setLastKeyUpValue: function(keyValue){
      this.set('lastKeyUpValue',keyValue);
    },
    changeFilter: function(){
      this.applyFilters();
    },
    inputKeyUp: function(value) {
      this.set('targetFilterValue', value);
      this.set('lastKeyUpTime', (new Date()).getTime());
    },
    clear: function(apply) {
      this.set('isClearing', true);

      this.get('filters').forEach(function(filter){
        let defaultValue = filter.defaultValue;

        if (filter.type === 'checkbox') {
          filter.set('checkboxValue', defaultValue ? defaultValue : false);
        } else if (filter.type === 'select') {
          filter.set('selectValue', defaultValue ? defaultValue : null);
        } else if (filter.type === 'multiselect') {
          filter.set('multiselectValue', defaultValue ? defaultValue : A([]));
        } else if (filter.type === 'date') {
          filter.set('dateValue', defaultValue ? defaultValue : null);
        } else {
          filter.set('searchVal', defaultValue ? defaultValue : null);
        }
      });

      this.set('isClearing', false);

      if (apply) {
        this.applyFilters();
      }
    }
  }
});
