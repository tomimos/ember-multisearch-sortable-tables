import Component from '@ember/component';

export default Component.extend({
  tagName: 'div',
  classNames: null,
  iconAltClass: '',
  icon: 'asterisk',
  title: 'Undefined',
  init() {
    this._super(...arguments);
    this.set('classNames', ['grid-key']);
  },
});
