import EmberObject from '@ember/object';
import MultisearchFilterMixinMixin from 'ember-multisearch-sortable-tables/mixins/multisearch-filter-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | multisearch-filter-mixin', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let MultisearchFilterMixinObject = EmberObject.extend(MultisearchFilterMixinMixin);
    let subject = MultisearchFilterMixinObject.create();
    assert.ok(subject);
  });
});
