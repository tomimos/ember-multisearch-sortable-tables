import EmberObject from '@ember/object';
import SortableTableColumnMixinMixin from 'ember-multisearch-sortable-tables/mixins/sortable-table-column-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | sortable-table-column-mixin', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let SortableTableColumnMixinObject = EmberObject.extend(SortableTableColumnMixinMixin);
    let subject = SortableTableColumnMixinObject.create();
    assert.ok(subject);
  });
});
